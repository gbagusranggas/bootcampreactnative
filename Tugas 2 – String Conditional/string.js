// SOAL NO.1

var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';

console.log(word)
console.log(second)
console.log(third)
console.log(fourth)
console.log(fifth)
console.log(sixth)
console.log(seventh)

// SOAL NO.2

var sentence = "I am going to be React Native Developer"; 

var exampleFirstWord = sentence[0]; 
var exampleSecondWord = sentence[2] + sentence[3]; 
var thirdWord  = sentence[4] + sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9];  // lakukan sendiri 
var fourthWord = sentence[10] + sentence[11] + sentence[12]; // lakukan sendiri 
var fifthWord = sentence[13] + sentence[14] + sentence[15]; // lakukan sendiri 
var sixthWord = sentence[16] + sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21]; // lakukan sendiri 
var seventhWord = sentence[22] + sentence[23] + sentence[24] + sentence[25] + sentence[26] + sentence[27] + sentence[28]; // lakukan sendiri 
var eighthWord = sentence[29] + sentence[30] + sentence[31] + sentence[32] + sentence[33] + sentence[34] + sentence[35] + sentence[36] + sentence[37] + sentence[38]; // lakukan sendiri 

console.log('First Word: ' + exampleFirstWord); 
console.log('Second Word: ' + exampleSecondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 
console.log('Eighth Word: ' + eighthWord);

// SOAL 3
var sentence3 = 'wow JavaScript is so cool'; 

var exampleFirstWord3 = sentence3.substring(0, 3); 
var secondWord3 = sentence3.substring(4, 15); // do your own! 
var thirdWord3 = sentence3.substring(16, 20); // do your own! 
var fourthWord3 = sentence3.substring(21, 25); // do your own! 

var firstWordLength = exampleFirstWord3.length
var secondWordLength = secondWord3.length
var thirdWordLength = thirdWord3.length
var fourthWordLength = fourthWord3.length  
// lanjutkan buat variable lagi di bawah ini 
console.log('First Word: ' + exampleFirstWord3 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWordLength); 
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWordLength); 
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWordLength); 



