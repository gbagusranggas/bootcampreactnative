//No 1
console.log("NO. 1");

function arrayToObject(arr) {
	var now = new Date();
	var thisYear = now.getFullYear(); // 2020 (tahun sekarang)
	var result = [];
	for (var i = 0; i < arr.length ; i++) {
		var temp = {
			firstname: arr[i][0],
			lastName: arr[i][1],
			gender: arr[i][2],
			age: thisYear - (arr[i][3] * 1)
		}

	var res = i + 1 + ". " + arr[i][0] + " "+arr[i][1];
	var dataTemp = res + ":" + " { firstname:"+ (temp.firstname)+", lastName:"+(temp.lastName) + ", gender:"+(temp.gender)+", age:"+(temp.age)+"}";
	result.push(dataTemp);
	}
	return result;
}

var input = [["Abduh", "Muhamad", "male", 1992], ["Ahmad", "Taufik", "male", 1985]];
console.log(arrayToObject(input));

//No 2
console.log("");
console.log("NO. 2");


function shoppingTime(memberId, money) {
	var result;
	var barang =[["Sepatu Stacattu", 1500000],["Baju Zoro",500000],["Baju H&N", 250000], ["Sweater Uniklooh", 175000],["Casing Handphone", 50000]];
	for (var i = 0; i < barang.length ; i++) {
		var tmp = {
			produk: barang[i][0],
			harga:barang[i][1]
		};
		var ab = "'"+tmp.produk+"'";
		var bar =[];
		if (memberId == null && money == null || memberId == '' && money != null) {
			result = "Mohon maaf, toko X hanya berlaku untuk member saja";

		}else if(memberId != null && money != null){
			if(money < barang[i][1]){
				result = "Mohon maaf, uang tidak cukup";

			}else{
				changeMoney = money - barang[i][1];
				
				result = "{ memberId:" + memberId+", money:" + money + ", listPurchased:"+"["+tmp.produk+"]" +", changeMoney:"+ changeMoney+"}";
			
			}
		} 
	}

	return result;
}

console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

//No 3
console.log("");
console.log("NO. 3");

function naikAngkot(arrPenumpang) {
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  var bayar=0;
  var result =[];
  for (var i = 0; i < arrPenumpang.length ; i++) {
		var temp = {
			penumpang: arrPenumpang[i][0],
			naikDari: arrPenumpang[i][1],
			tujuan: arrPenumpang[i][2],
		}
	if(temp.naikDari == rute[0] && temp.tujuan == rute[1]){
		bayar=2000;
	}else if(temp.naikDari == rute[0] && temp.tujuan == rute[2]){
		bayar= 4000;
	}else if(temp.naikDari == rute[0] && temp.tujuan == rute[3]){
		bayar = 6000;
	}else if(temp.naikDari == rute[0] && temp.tujuan == rute[4]){
		bayar =8000;
	}else if(temp.naikDari == rute[0] && temp.tujuan == rute[5]){
		bayar=10000;
	}	
	var dataTemp = " { penumpang:"+ (temp.penumpang)+", naikDari:"+(temp.naikDari) + ", tujuan:"+(temp.tujuan)+", bayar:"+bayar+"}";
	result.push(dataTemp);

}
return result;
}
console.log(naikAngkot([['Dimitri', 'A', 'B'], ['Icha', 'A', 'F']]));
