import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    ScrollView,
    FlatList,
    TextInput,
    Button

} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default LoginScreen = () => {
    return (
        <View style={styles.container}>
            <Image source = {require('./images/logo (1).png')}
             style={styles.logoSection} />
                 
            <View style={styles.titleSection}></View>
            <View style={styles.formSection}>

            <Text style={styles.labelInput}>Username / Email</Text>
            <TextInput style={styles.inputTextForm} />

            <Text style={styles.labelInput}>Password</Text>
            <TextInput style={styles.inputTextForm} />

            <View style={styles.buttonSection}>
            <Button title='Masuk' color="#3EC6FF" style={{ borderRadius:20 }} />
            <Text style={{ textAlign:'center',
            marginBottom:20,
        marginTop:20 }}>Atau</Text>
        <Button title='Daftar' color="#003366" />

        </View>
</View>
        </View>
    )
}

const styles = StyleSheet.create({
    container : {
        flex: 1,
        backgroundColor: '#FFFFFF',
    },
    logoSection : {
        top:63,
    },
    titleSection: {
        alignItems: 'center',
        top:100,
        height:30,
    },
    titleScreen: {
        color: '#003366',
        fontSize:24,
        height:30
    },
    formSection: {
        top:130,
        color: '#003366',
        fontSize:16,
        fontFamily: 'Roboto',
        flexDirection: 'column'
    },
    labelInput: {
        left:41,
        marginBottom:5
    },
    inputTextForm: {
        height:40,
        borderColor: 'gray',
        borderWidth:1,
    }
})