// No 1
console.log("NO. 1");
console.log("LOOPING PERTAMA");
var loop1 = 1;
var word1 ="I love coding";

while(loop1 <= 20 ) { 
  if(loop1 % 2 == 0){
    console.log(loop1 + " - "+ word1);
  }
  loop1++;
} 

console.log("LOOPING KEDUA");
var loop2 = 20;
var word2 ="I will become a mobile developer";

while(loop2 >= 1 ){ 
  if(loop2 % 2 == 0){
    console.log(loop2 + " - "+ word2);
  }
  loop2--;
} 

// No 2
console.log("");
console.log("NO. 2");
for(var i=1; i <= 20; i++){
  if( i % 2 == 1 && i % 3 == 1){
    console.log(i+" - "+"Santai");

  }else if(i % 2 == 0){
    console.log(i+" - "+"Berkualitas");

  }else if(i % 2 == 1 && i % 3 == 0 ){
    console.log(i+" - "+"I Love Coding");
  }
}

// No 3
console.log("");
console.log("NO. 3");
for(var i=1; i <= 4; i++){
  var x = '';
  for(var j=1; j <= 8 ; j++){
   var x = x + "#";
 }
 console.log(x);
}

// No 4
console.log("");
console.log("NO. 4");
for(var i = 1; i <= 7; i++) {
  var x = '';
  for(var j = 1; j <= i; j++) {
    var x = x + "#";
  }
  console.log(x);
}

// No 5
console.log("");
console.log("NO. 5");

for(var i = 1; i <= 8; i++) {
  var x = '';
  if (i%2 == 0){
      var x = x + "#";
    }
  for(var j = 1; j <= 8; j++) {
    if (j%2 == 0){
      var x = x + "#";
    }else{
       var x = x + " ";
    }
    
  }
  console.log(x);
}