
// No.1

goldenFunction = () => {
   console.log("this is golden!!")
} 

goldenFunction();

// No.2

const fullName = 'William, Imoh'
 
const name = {fullName}

console.log(name)

// No.3

const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

const {firstName, lastName, destination, occupation, spell} = newObject;
console.log(firstName, lastName, destination, occupation);

// No.4

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

const combined = [west,east];
//Driver Code
console.log(combined)

// No. 5

const planet = 'earth'
const view = 'glass'

var before = ` Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor 
incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`

console.log(before)