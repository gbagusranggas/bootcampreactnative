// 1. ANIMAL

class Animal {
    constructor(name,legs)
    {
    	this._name = name;
    	this._legs = legs;
    }
    get name()
    {
    	return this._name;
    	return this._legs;
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

// Relase 1

class Model extends Animal {
	constructor(name,legs)
	{
		super(name);
		this.name

		function yell()
		{
			console.log('Auooo');
		}
		function jump()
		{
			console.log("hop hop");
		}
	}

}
 
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

// No.2

class Clock {
    // Code di sini
}

var clock = new Clock({template: 'h:m:s'});
clock.start(); 