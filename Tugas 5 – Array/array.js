//No 1
console.log("NO. 1");

function range(startNum, finishNum) {
	if(startNum == null || finishNum == null){
		return -1;
		
	} else {
		if (startNum < finishNum ){
			var i =[];
			while (startNum <= finishNum){
				i.push(startNum);
				startNum++;	
			}
			return i;

		} else {
			var i =[];
			while (startNum >= finishNum){
				i.push(startNum);

				startNum--;	
			}
			return i;
		}
	}
}

console.log(range(1, 10)); //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)); // -1
console.log(range(11,18)); // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)); // [54, 53, 52, 51, 50]
console.log(range()); // -1 

//No 2
console.log("");
console.log("NO. 2");

function rangeWithStep(startNum, finishNum,step) {
	if (startNum < finishNum ){
		var i =[];
		while (startNum <= finishNum){
			i.push(startNum);
			startNum+=step;	
		}
		return i;

	} else {
		var i =[];
		while (startNum >= finishNum){
			i.push(startNum);

			startNum-=step;	
		}
		return i;
	}
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

//No 3
console.log("");
console.log("NO. 3");

function sum(startNum, finishNum,step) {
	var i =[];
	var sum =0;

	if (step == null && startNum != null && finishNum != null ){
		step=1;
	} else if (step == null && startNum != null && finishNum == null){
		step=1;
		finishNum=0;
	}
	
	if (startNum < finishNum ){
		while (startNum <= finishNum){
			i.push(startNum);
			startNum+=step;	
		}

	} else {
		while (startNum >= finishNum){
			i.push(startNum);
			startNum-=step;	
		}
	}
	
	var tmpArr=i;
	for (var y=0; y < tmpArr.length; y++) {
		sum += tmpArr[y];	
	}
	return sum;
	
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

//No 4
console.log("");
console.log("NO. 4");

function dataHandling(id, name, address, ttl, hobby) {
	var arr = [id,name,address,ttl,hobby];
	
	var input = [
	["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
	["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
	["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
	["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
	];

	input.push(arr);

	for (var i = 0; i< input.length; i++) {
		for (var j = 0; j < input[i].length; j++) { 
			var k = input[i][0];
			var h = input[i][1];
			var m = input[i][2];
			var n = input[i][3];
			var o = input[i][4];
		}
		console.log ("Nomor ID:" + k);
		console.log ("Nama Lengkap:" + h);
		console.log ("TTL:" + m + " "+n);
		console.log ("Hobi :" + o);
		console.log("");
	}
	return "";
}

console.log(dataHandling("0005", "Rangga", "Bogor", "02/05/1997", "Business"));

//No 5 
console.log("");
console.log("NO. 5");

function balikKata(kata) {
	var kataLama = kata;
	var kataBaru = '';
	for (var i = kata.length - 1; i >= 0; i--) {
		kataBaru = kataBaru + kataLama[i];
	}

	return kataBaru;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

//No 6
console.log("");
console.log("NO. 6");
function dataHandling2(id, name, address, ttl, gender, school) {

	input= [];	
	var tmpTtl = ttl.split("/");
	tmpTtl.sort(function (value1, value2) { return value2 - value1 } ) ;
	var slug = tmpTtl.join("-")
	input.splice(0, 0,id, name, address, ttl, gender, school);

	switch (tmpTtl[1]){
		case '1': 
			bulan = "Januari";
			break;
		case '2':
			bulan = "Februari";
			break;
		case '3':
			bulan = "Maret";
			break;
		case '4':
			bulan = "April";
			break;
		case '5':
			bulan = "Mei";
			break;
		case '6':
			bulan = "Juni";
			break;
		case '7':
			bulan = "Juli";
			break;
		case '8':
			bulan = "Agustus";
			break;
		case '9':
			bulan = "September";
			break;
		case '10':
			bulan = "Oktober";
			break;
		case '11':
			bulan = "November";
			break;
		case '12':
			bulan = "Desember";
			break;
	}

	console.log(input);
	console.log(bulan);
	console.log(tmpTtl);
	console.log(slug);
	console.log(input[1].slice(0,15));
	return "";
}

console.log(dataHandling2("001","Gusti Bagus Rangga Saputra","Bogor","02/5/1997","pria","SMA NEGERI 16 SURABAYA"));
